<?php

namespace App\Controller;

use App\Entity\Movie;
use App\Repository\MovieRepository;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\View\View;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations as Rest;


class MovieController extends AbstractController
{
    /**
     * @Route("/movie", name="movie")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/MovieController.php',
        ]);
    }

    /**
     * Create a Movie
     * @Rest\Post("/add_movie")
     * @param Request $request
     * @param MovieRepository $movieRepository
     * @param Movie $movie
     * @return View
     */
    public function addMovie(Request $request,EntityManager $em): View
    {
        $movie = new Movie();
        $movie->setName($request->get('name'));
        $movie->setDescription($request->get('description'));
//        $entityManager = $this->getDoctrine()->getManager();
//        $em->getRepository('Movie')->save($movie);

//        $em->persist($movie);
//        $em->flush();

        // In case our POST was a success we need to return a 201 HTTP CREATED response
        return View::create($movie, Response::HTTP_CREATED);

    }

}
