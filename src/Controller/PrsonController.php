<?php

namespace App\Controller;

use App\Entity\Person;
use App\Service\PersonService;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PrsonController extends AbstractController
{
    /**
     * @Route("/person", name="prson")
     */
    public function index()
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/PrsonController.php',
        ]);
    }

    /**
     * @Route("/display", name="display",methods="GET")
     */
    public function displayAll(PersonService $service)
    {
        //http://127.0.0.1:8000/display
        $result = $service->getAllPersons();
        //var_dump($result);
        return $this->json($result);
    }

    /**
     * @Route("/add",methods={"POST","HEAD"})
     */
    public function addPerson(Request $request, PersonService $service)
    {
        // http://127.0.0.1:8000/add

        //ci-dessous le format json a ecrire dans postman

        //    {
        //        "firstName": "Hachem",
        //        "lastName": "Taher Ali",
        //        "age": 27
        //    }

        $entityManager = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);
        $result = $service->addPerson($entityManager, $data);
        return $this->json($result);
    }

    /**
     * @Route("/delete/{id}",methods={"DELETE"})
     */
    public function removePerson($id, PersonService $service)
    {
        //http://127.0.0.1:8000/delete/16
        $entityManager = $this->getDoctrine()->getManager();
        $result = $service->deletePerson($entityManager, $id);
        return $this->json($result);
    }

    /**
     * @Route("/edit/{id}",methods={"PUT"})
     */
    public function editPerson(Request $request, $id, PersonService $service)
    {
        //http://127.0.0.1:8000/edit/16
        $entityManager = $this->getDoctrine()->getManager();
        $data = json_decode($request->getContent(), true);
        $result = $service->editPerson($entityManager, $id, $data);
        return $this->json($result);
    }

    /**
     * @Route("/findById/{id}",methods="GET")
     */
    public function findPersonById(PersonService $service, $id)
    {
        //http://127.0.0.1:8000/findById/16
        $result = $service->findPersonById($id);
        return $this->json($result);
    }
}