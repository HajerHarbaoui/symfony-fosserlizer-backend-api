<?php


namespace App\Service;


use App\Entity\Person;
use App\Repository\PersonRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;

class PersonService
{

    protected $PersonRepository ;
    public function __construct(PersonRepository $PersonRepository)
    {
        $this->PersonRepository = $PersonRepository;
    }

    public function getAllPersons()
    {
        $result = $this->PersonRepository->findAll();
        return $result;
    }

    public function addPerson(EntityManager $em,$data)
    {
        $person = new Person();
        $person->setFirstName($data["firstName"]);
        $person->setLastName($data["lastName"]);
        $person->setLastName($data["age"]);
        $em->persist($person);
        $em->flush();
        return ;
    }

    public function deletePerson(EntityManager $entityManager,$id)
    {
        $person = $this->PersonRepository->find($id);
        $entityManager->remove($person);
        $entityManager->flush();
        var_dump($person);
        return;
    }

    public function editPerson(EntityManager $entityManager,$id,$data)
    {
        $person = $this->PersonRepository->find($id);
        $person->setFirstName($data["firstName"]);
        $person->setLastName($data["lastName"]);
        $person->setAge($data["age"]);
        $entityManager->persist($person);
        $entityManager->flush();
        var_dump($person);
        return;
    }

    public function findPersonById($id)
    {
        $person = $this->PersonRepository->find($id);
        return($person);
    }

}